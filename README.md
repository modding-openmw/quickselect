# Quickselect

OpenMW Dev build(0.49) required

**Before installing this mod, you should unbind all the vanilla hotkeys!**

Quickselect adds a hotbar to the UI, with custom behaviour. 

You can either have it show at all times, or only show when needed. It allows for a more controller friendly experience, the shoulder buttons/dpad left/right allow you to select which slot to use, then A to use it.

The mod re-implements the vanilla favorite menu(F1), with some enhancements. 

If you press a hotkey which has an item equipped already, it will unequip it.

You can have 3 hotbars. Press Shift+1, or use the DPad up and down buttons to choose your hotbar. You can also use the -/= keys or the [/] keys, but you must unbind them from their default purpose.

You can use the DPad/Shoulder buttons to select which item you'd like to equip. If enabled, you can also use the arrow keys for this.

Check the settings, you can customize behaviour from there.

#### Credits

Author: ZackHasaCat

#### Installation

1. Download the mod from the above link, release or dev version.
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\quickselect

        # Linux
        /home/username/games/OpenMWMods/quickselect

        # macOS
        /Users/username/games/OpenMWMods/quickselect

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\quickselect"`)
1. Add  `content=quickselect.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/quickselect/-/issues)
* Email `zack@iwsao.com`
* Contact the author on Discord: `@ZackHasaCat`
